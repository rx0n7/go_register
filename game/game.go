package game

import (
	"rx0n7/database"

	"github.com/gofiber/fiber"
	"gorm.io/gorm"
)

type Game struct {
	gorm.Model
	Pwd    int    `json:"pwd"`
	Name   string `json:"name"`
	Online string `json:"online"`
}

func GetGames(c *fiber.Ctx) {
	db := database.DBConn
	var games []Game
	db.Find(&games)
	c.JSON(games)
}

func GetGame(c *fiber.Ctx) {
	id := c.Params("id")
	db := database.DBConn
	var game Game
	db.Find(&game, id)
	c.JSON(game)
}

func NewGame(c *fiber.Ctx) {
	db := database.DBConn

	game := new(Game)
	if err := c.BodyParser(game); err != nil {
		c.Status(503).Send(err)
		return
	}
	db.Create(&game)
	c.JSON(game)
}

func DeleteGame(c *fiber.Ctx) {
	id := c.Params("id")
	db := database.DBConn

	var game Game
	db.First(&game, id)
	if game.Name == "" {
		c.Status(500).Send("Nenhum game enconrado com esse ID ")
		return
	}
	db.Delete(&game)
	c.Send("Sucesso game deletado! ")
}
