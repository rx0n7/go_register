module rx0n7

go 1.16

require (
	github.com/gofiber/fiber v1.14.6
	github.com/klauspost/compress v1.11.13 // indirect
	github.com/valyala/fasthttp v1.23.0 // indirect
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.20.7
)
