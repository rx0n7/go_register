package main

import (
	"fmt"
	"rx0n7/database"
	"rx0n7/game"

	"github.com/gofiber/fiber"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func setupRoutes(app *fiber.App) {
	app.Get("/api/v1/game", game.GetGames)
	app.Get("/api/v1/game/:id", game.GetGame)
	app.Post("/api/v1/game", game.NewGame)
	app.Delete("/api/v1/game/:id", game.DeleteGame)
}

func initDatabase() {
	var err error
	database.DBConn, err = gorm.Open(sqlite.Open("games.db"), &gorm.Config{})
	if err != nil {
		panic("Falhou a conexao")
	}
	fmt.Println("Conectado com sucesso")

	database.DBConn.AutoMigrate(&game.Game{})
	fmt.Println("Database Migrated")
}

func main() {
	app := fiber.New()
	initDatabase()

	setupRoutes(app)

	app.Listen(3000)

}
